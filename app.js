const express=require('express')
const path=require('path')
const mongoose=require('mongoose')
const bodyParser=require('body-parser')

//connecting to db using mongoose
mongoose.connect('mongodb://localhost/nodekb')
let db=mongoose.connection;

//check for db errors
db.once('open',function(){
    console.log('Connected to MongoDB')
})

//check for db errors
db.on('error',function(err){
    console.log(err)
})

//init app
const app=express();

//Bring in models
let Article =require('./models/article')

//load view engine
app.set('views',path.join(__dirname,'views'))
app.set('view engine','pug')

//set public folder
app.use(express.static(path.join(__dirname,'public')))

//Body Parser Middleware
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

//home route
app.get("/",function(req,res){
    Article.find({},function(err,articles){
        if(err){
            console.log(err)
        }
        else{
            res.render('index',{
                title: 'Articles',
                articles: articles
            })
        }
    })
   
});

//add submit post route
app.post('/articles/add',function(req,res){
   let article=new Article();
   article.title=req.body.title
   article.author=req.body.author
   article.body=req.body.body

   article.save(function(err){
       if(err){
           console.log(err)
           return
       }
       else{
           res.redirect('/')
       }
   })
})


//article route
app.get("/articles/add",function(req,res){
    res.render('add_article',{
        title: 'Add Article'
    });
});

//start server
app.listen(3000,function(){
    console.log('Server started on port 3000...')
})